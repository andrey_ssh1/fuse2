unit cirquits;

{$mode delphi}{$H+}

interface

uses
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
    Menus;

type
    TSettingsOpen = procedure(n: TTreeNode) of object;

    { Tcirquits_form }

    Tcirquits_form = class(TForm)
        fTV: TTreeView;
        Add_fuse: TMenuItem;
        Add_cable: TMenuItem;
        Add_bus: TMenuItem;
        Fault: TMenuItem;
        Select_Accumulator: TMenuItem;
        Rename: TMenuItem;
        PopupMenu: TPopupMenu;
        procedure Add_busClick(Sender: TObject);
        procedure Add_cableClick(Sender: TObject);
        procedure Add_fuseClick(Sender: TObject);
        procedure FaultClick(Sender: TObject);
        procedure FormCreate(Sender: TObject);
        procedure fTVDblClick(Sender: TObject);
        procedure PopupMenuPopup(Sender: TObject);
        procedure RenameClick(Sender: TObject);
    private
        { private declarations }
    public
        SettingsOpen: TSettingsOpen;
        AddCable: TNotifyEvent;
        AddFuse: TNotifyEvent;
        AddBus: TNotifyEvent;
        AddFault: TNotifyEvent;
        { public declarations }
    end;

var
    cirquits_form: Tcirquits_form;

    TV: TTreeView = nil;

implementation

{$R *.lfm}

{ Tcirquits_form }

procedure Tcirquits_form.FormCreate(Sender: TObject);
begin
    TV := fTV;
end;

procedure Tcirquits_form.Add_fuseClick(Sender: TObject);
begin
    AddFuse(TV.Selected);
end;

procedure Tcirquits_form.FaultClick(Sender: TObject);
begin
    AddFault(TV.Selected);
end;

procedure Tcirquits_form.Add_cableClick(Sender: TObject);
begin
    AddCable(TV.Selected);
end;

procedure Tcirquits_form.Add_busClick(Sender: TObject);
begin
    AddBus(TV.Selected);
end;

procedure Tcirquits_form.fTVDblClick(Sender: TObject);
begin
    SettingsOpen(TV.Selected);
end;

procedure Tcirquits_form.PopupMenuPopup(Sender: TObject);
begin
    Select_Accumulator.Visible := TV.Selected=TV.Items[0];
end;

procedure Tcirquits_form.RenameClick(Sender: TObject);
begin
    TV.Selected.EditText;
end;

end.

