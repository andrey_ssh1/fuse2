unit settings;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    StdCtrls, MaskEdit, Spin;

type

    TSettingsChange = procedure (name: unicodestring; o: integer) of object;

    TSettingSelection = class
        i,o: integer;
        f: double;
    end;

    { TFloatPanel }

    TFloatPanel = class(TGroupBox)
        fEdit: TFloatSpinEdit;
        constructor Create(AOwner: TWinControl);
    end;

    { Tsettings_form }

    Tsettings_form = class(TForm)
        procedure FormDeactivate(Sender: TObject);
    private
        fBlanks: array of TCustomGroupBox;
        fCallback: TNotifyEvent;
        procedure SelectionChanged(Sender: TObject);
        { private declarations }
    public
        constructor Create(callback: TNotifyEvent);
        procedure add_blank(i: integer; name: unicodestring; opt: TStrings);
        procedure add_blank_float(i: integer; name: unicodestring);
        { public declarations }
    end;


implementation

{$R *.lfm}

{ TFloatPanel }

constructor TFloatPanel.Create(AOwner: TWinControl);
begin
    inherited Create(AOwner);
    self.Parent := AOwner;
    fEdit := TFloatSpinEdit.Create(self);
    fEdit.Parent := self;
    fEdit.Align:=alTop;
    fEdit.Value:=1;
    fEdit.DecimalPlaces:=1;
    fEdit.MinValue:=0.1;
    fEdit.MaxValue:=999.9;
    fEdit.Width:= 60;
    self.AutoSize:=true;
end;


{ Tsettings_form }


procedure Tsettings_form.FormDeactivate(Sender: TObject);
var i: integer;
begin
    for i := 0 to high(fBlanks) do
        if fBlanks[i] is TFloatPanel then
            (fBlanks[i] as TFloatPanel).fEdit.EditingDone;
end;


procedure Tsettings_form.SelectionChanged(Sender: TObject);
var ss: TSettingSelection;
begin
  ss := TSettingSelection.Create;
  ss.i:= (Sender as TComponent).Tag;
  if sender is TRadioGroup then ss.o := (Sender as TRadioGroup).ItemIndex;
  if sender is TFloatSpinEdit then ss.f := (Sender as TFloatSpinEdit).Value;
  fCallback(ss);
  ss.Free;
end;

constructor Tsettings_form.Create(callback: TNotifyEvent);
begin
    Inherited Create(nil);
    fCallback := callback;
end;

procedure Tsettings_form.add_blank(i: integer; name: unicodestring;
    opt: TStrings);
var rg: TRadioGroup;
begin
    rg := TRadioGroup.Create(self);
    rg.Parent := self;
    rg.Caption:=name;
    rg.Items := opt;
    if opt.Count>0 then rg.ItemIndex:=0;
    rg.Align:=alRight;
    rg.AutoSize:=true;
    rg.Visible:=true;
    rg.Tag:=i;
    rg.OnSelectionChanged := SelectionChanged;

    SetLength(fBlanks, Length(fBlanks)+1);
    fBlanks[high(fBlanks)] := rg;
end;

procedure Tsettings_form.add_blank_float(i: integer; name: unicodestring);
var bf: TFloatPanel;
begin
    bf := TFloatPanel.Create(self);
    bf.Caption:=name;
    bf.Align:=alBottom;
    bf.fEdit.Tag:=i;
    bf.fEdit.OnEditingDone:=SelectionChanged;

    SetLength(fBlanks, Length(fBlanks)+1);
    fBlanks[high(fBlanks)] := bf;
end;

end.

