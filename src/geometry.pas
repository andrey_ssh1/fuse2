unit geometry;

{$mode delphi}{$H+}

interface

uses
    Classes, SysUtils;

type
    TPointFloat = record case byte of 0: (x,y: real); 1: (U,R: real); 2:(I,t: real); end;
    TPointsFloat = array of TPointFloat;
    TMatrix = array[1..3,1..3] of real;

const
    identity: TMatrix = ((1,0,0),(0,1,0),(0,0,1));

function fpoint(x,y: real): TPointFloat;
function mul(const p: TPointFloat; const m: TMatrix): TPointFloat; overload;
function mul(const m1: TMatrix; const m2: TMatrix): TMatrix; overload;

implementation

function fpoint(x,y: real): TPointFloat;
begin
    result.X := x;
    result.Y := y;
end;

function mul(const p: TPointFloat; const m: TMatrix): TPointFloat; overload;
begin
    result.x := p.x*m[1,1]+p.y*m[2,1]+1*m[3,1];
    result.y := p.x*m[1,2]+p.y*m[2,2]+1*m[3,2];
end;

function mul(const m1: TMatrix; const m2: TMatrix): TMatrix; overload;
begin
    result[1,1] := m1[1,1]*m2[1,1]+m1[1,2]*m2[2,1]+m1[1,3]*m2[3,1];
    result[1,2] := m1[1,1]*m2[1,2]+m1[1,2]*m2[2,2]+m1[1,3]*m2[3,2];
    result[1,3] := m1[1,1]*m2[1,3]+m1[1,2]*m2[2,3]+m1[1,3]*m2[3,3];

    result[2,1] := m1[2,1]*m2[1,1]+m1[2,2]*m2[2,1]+m1[2,3]*m2[3,1];
    result[2,2] := m1[2,1]*m2[1,2]+m1[2,2]*m2[2,2]+m1[2,3]*m2[3,2];
    result[2,3] := m1[2,1]*m2[1,3]+m1[2,2]*m2[2,3]+m1[2,3]*m2[3,3];

    result[3,1] := m1[3,1]*m2[1,1]+m1[3,2]*m2[2,1]+m1[3,3]*m2[3,1];
    result[3,2] := m1[3,1]*m2[1,2]+m1[3,2]*m2[2,2]+m1[3,3]*m2[3,2];
    result[3,3] := m1[3,1]*m2[1,3]+m1[3,2]*m2[2,3]+m1[3,3]*m2[3,3];
end;

function translate(const v: TPointFloat): TMatrix;
begin
    result := identity;
    result[1,3]:=v.x;
    result[2,3]:=v.y;
end;

end.

