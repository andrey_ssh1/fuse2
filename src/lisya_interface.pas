﻿unit lisya_interface;

{$mode delphi}{$H+}

interface

uses
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    Classes, SysUtils, Forms, Dialogs
    , geometry
    , settings
    , dlisp_eval
    , lisya_streams
    , lisia_charset
    , dlisp_values
    , dlisp_read
    , lisya_predicates
    , lisya_symbols
    ;

type

    { TSetting }

    TSetting = class
        name: unicodestring;
        options: TValue;
        value: TValue;
        m_unit: unicodestring;
        fPanel: TCustomGroupBox;
        constructor Create(n: TVSymbol; o: TValue; _u: unicodestring);
        destructor Destroy;
    end;

    { TSettings }

    TSettings = class
        s: array of TSetting;
        fSettingsPanel: TSettings_form;
        fParent: TObject;
        constructor Create(name: unicodestring; _s: TVList);
        destructor Destroy;
        procedure Change(sender: TObject);
        procedure Show;
        function GetValue(name: unicodestring): TValue;
        function GetFloat(name: unicodestring): double;
        function GetString(name: unicodestring): unicodestring;
        function AsList: TVList;
        procedure Read(L: TVList);
    end;

    { TAdjustableObject }

    TAdjustableObject = class
        model: unicodestring;
        fDefinition: TVList;
        settings: TSettings;
        function call(expr: TVList): TValue;
        procedure ShowSettingsPanel;
        procedure Read(fn: unicodestring); overload;
        procedure Read(_d: TVList); overload;
        destructor Destroy; override;
    end;

    { TFuse }

    TFuse = class (TAdjustableObject)
        fCurveExpr: TVList;
        function CalcCurve: TPointsFloat;
        constructor Create(fn: unicodestring);
        destructor Destroy; override;
    end;

    { TAccumulator }

    TAccumulator = class (TAdjustableObject)
        fMaxModeExpr: TVList;
        fMinModeExpr: TVList;
        function MaxMode: TPointFloat;
        function MinMode: TPointFloat;
        constructor Create(fn: unicodestring); overload;
        constructor Create; overload;
        destructor Destroy; override;
    end;

    { TElementRec }

    TElementRec = class
        name: unicodestring;
        definition: TVList;
        settings: TVList;
        children: array of TElementRec;
        constructor Create(_n: unicodestring; _o: TAdjustableObject); overload;
        constructor Create(_n, _d: unicodestring; _s: TSettings); overload;
        constructor Read(r: TVRecord);
        constructor LoadFromFile(fn: unicodestring);
        destructor Destroy;
        procedure AddChild(ch: TElementRec);
        procedure SaveToFile(fn: unicodestring);
        function AsRecord: TVRecord;
        function ElementKind: unicodestring;
    end;

function list_to_points(L: TVList): TPointsFloat;

var
    notify_change: TNotifyEvent = nil;

const
    cable_params = '( '+
        '(Материал (Cu Al)) '+
        '(Сечение число "кв.мм") '+
        '(Длина число "м"))';

function cable_settings: TSettings;



implementation

var
    flow: TEvaluationFlow;
    sModel, sSettings, sCurve, sMaxMode, sMinMode, sFloat: TVSymbol;


function list_to_points(L: TVList): TPointsFloat;
var i: integer;
begin
    //if not vpListOfPoints(L) then raise Exception.Create(L.AsString);
    SetLength(result, L.Count);
    for i := 0 to L.high do result[i]:= fpoint(L.L[i].F[0], L.L[i].F[1]);
end;

function cable_settings: TSettings;
var V: TValue;
begin
    V := dlisp_read.read_from_string(cable_params);
    result := TSettings.Create('Кабель', V as TVList);
    V.Free;
end;

function assoc(key: TVSymbol; L: TVList): TVList;
var i: integer;
begin
   // ShowMessage(L.AsString);
    for i := 0 to L.high do
        if (L.look[i] is TVList) and (L.L[i].Count>0) and key.equal(L.L[i].look[0])
        then begin
            result := L.L[i].subseq(1) as TVList;
            Exit;
        end;
    result := TVList.Create;
end;


// ----- проверка структуры -------------

function vphSymbol(V: TValue; name: unicodestring): boolean;
begin result := (V is TVSymbol) and ((V as TVSymbol).uname=name); end;

function vpSymbol_FUSE(V: TValue): boolean;
begin result := vphSymbol(V, 'ПРЕДОХРАНИТЕЛЬ'); end;

function vpSymbol_MODEL(V: TValue): boolean;
begin result := vphSymbol(V, 'МОДЕЛЬ'); end;

function vpSymbol_SETTINGS(V: TValue): boolean;
begin result := vphSymbol(V, 'УСТАВКИ'); end;

function vpSymbol_CURVE(V: TValue): boolean;
begin result := vphSymbol(V, 'ХАРАКТЕРИСТИКА'); end;

function vpSymbol_ACCUMULATOR(V: TValue): boolean;
begin result := vphSymbol(V, 'АККУМУЛЯТОР'); end;

function vpSymbol_MAX_MODE(V: TValue): boolean;
begin result := vphSymbol(V, 'МАКСИМАЛЬНЫЙ-РЕЖИМ'); end;

function vpSymbol_MIN_MODE(V: TValue): boolean;
begin result := vphSymbol(V, 'МИНИМАЛЬНЫЙ-РЕЖИМ'); end;

function vpSymbol_FLOAT(V: TValue): boolean;
begin result := vphSymbol(V, 'ЧИСЛО'); end;


function vpStatement_MODEL(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).count=2)
        and vpSymbol_MODEL((V as TVList).look[0])
        and ((V as TVList).look[1] is TVString);
end;

function vpStatement_Options(V: TValue): boolean;
begin
    result := vpSymbol_FLOAT(V) or ((V is TVList) and ((V as TVList).Count>0));
end;

function vpStatement_Setting(V: TValue): boolean;
begin
    result := (V is TVList)
        and ((V as TVList).Count in [2,3])
        and tpOrdinarySymbol((V as TVList).look[0])
        and vpStatement_Options((V as TVList).look[1]);
end;

function vpStatement_SETTINGS(V: TValue): boolean;
var i: integer;
begin
    result := (V is TVList) and ((V as TVList).count>=1)
        and vpSymbol_SETTINGS((V as TVList).look[0]);
    if not result then Exit;
    for i := 1 to (V as TVList).high do begin
        result := vpStatement_Setting((V as TVList).look[i]);
        if not result then Exit;
    end;
end;

function vphExpression(V: TValue; head: TTypePredicate; count: integer=1): boolean;
begin
    result := (V is TVList) and ((V as TVList).count>=count)
        and head((V as TVList).look[0]);
end;

function vpStatement_CURVE(V: TValue): boolean;
begin result := vphExpression(V, vpSymbol_CURVE); end;

function vpStatement_MAX_MODE(V: TValue): boolean;
begin result := vphExpression(V, vpSymbol_MAX_MODE); end;

function vpStatement_MIN_MODE(V: TValue): boolean;
begin result := vphExpression(V, vpSymbol_MIN_MODE); end;


function vpStatement_FUSE(V: TValue): boolean;
var i: integer; ch: boolean;
begin
    result := vphExpression(V, vpSymbol_FUSE, 2);
    ch := false;
    if not result then Exit;
    for i := 1 to (V as TVList).high do begin
        result := vpStatement_MODEL((V as TVList).look[i])
            or vpStatement_SETTINGS((V as TVList).look[i])
            or vpStatement_CURVE((V as TVList).look[i]);
        if not result then Exit;
        if vpStatement_CURVE((V as TVList).look[i]) then ch := true;
    end;
    result := ch;
end;

function vpStatement_ACCUMULATOR(V: TValue): boolean;
var i: integer; max_m, min_m: boolean;
begin
    result := vphExpression(V, vpSymbol_ACCUMULATOR, 2);
    max_m := false;
    min_m := false;
    if not result then Exit;
    for i := 1 to (V as TVList).high do begin
        result := vpStatement_MODEL((V as TVList).look[i])
            or vpStatement_SETTINGS((V as TVList).look[i])
            or vpStatement_MAX_MODE((V as TVList).look[i])
            or vpStatement_MIN_MODE((V as TVList).look[i]);
        if not result then Exit;
        if vpStatement_MAX_MODE((V as TVList).look[i]) then max_m := true;
        if vpStatement_MIN_MODE((V as TVList).look[i]) then min_m := true;
    end;
    result := max_m and min_m;
end;

function vpPoint(V: TValue): boolean;
begin
    result := (V is TVList) and ((V as TVList).count=2)
        and ((V as TVList).look[0] is TVReal) and ((V as TVList).look[1] is TVReal);
end;

// ---------- извлечение -----------------

function extract_m_unit(L: TVList): unicodestring;
begin
    if L.Count=3
    then begin
        if tpString(L.look[2])
        then result := L.S[2]
        else result := L.look[2].AsString;
    end
    else result := '';
end;

{ TElementRec }

constructor TElementRec.Create(_n: unicodestring; _o: TAdjustableObject);
begin
    children := nil;
    name := _n;
    definition := _o.fDefinition.Copy as TVList;
    settings := _o.settings.AsList;
end;

constructor TElementRec.Create(_n, _d: unicodestring; _s: TSettings);
begin
    children := nil;
    name := _n;
    definition := TVList.Create([TVSymbol.Create(_d)]);
    settings := _s.AsList;
end;

destructor TElementRec.Destroy;
var i: integer;
begin
    definition.Free;
    settings.Free;
    for i := 0 to high(children) do FreeAndNil(children[i]);
end;

procedure TElementRec.AddChild(ch: TElementRec);
begin
    SetLength(children, Length(children)+1);
    children[high(children)] := ch;
end;

procedure TElementRec.SaveToFile(fn: unicodestring);
var sp: TVStreamPointer;
begin
    sp := TVStreamPointer.Create(TLFileStream.Create(fn, fmCreate, lisia_charset.seUTF8));
    dlisp_read.print(AsRecord,sp);
    sp.Free;
end;

function TElementRec.AsRecord: TVRecord;
var i: integer; ch: TVList;
begin
    result := TVRecord.Create;
    result.AddSlot('имя',TVString.Create(name));
    result.AddSlot('определение',definition.Copy);
    if settings<>nil
    then result.AddSlot('уставки',settings.Copy)
    else result.AddSlot('уставки', TVList.Create);
    ch := TVList.Create;
    for i := 0 to high(children) do ch.Add(children[i].AsRecord);
    result.AddSlot('дочерние',ch);
end;

function TElementRec.ElementKind: unicodestring;
begin
    result := definition.SYM[0].name;
end;

constructor TElementRec.Read(r: TVRecord);
var L: TVList; i: integer;
begin
    name := (r.look_name['имя'] as TVString).S;
    definition := r.look_name['определение'].Copy as TVList;
    settings := r.look_name['уставки'].Copy as TVList;
    L := r.look_name['дочерние'] as TVList;
    children := nil;
    for i := 0 to L.high do AddChild(TElementRec.Read(L.look[i] as TVRecord));
end;

constructor TElementRec.LoadFromFile(fn: unicodestring);
var s: TLFileStream;
begin
    s := TLFileStream.Create(fn, fmOpenRead, lisia_charset.seUTF8);
    Read(dlisp_read.read(s) as TVRecord);
    s.Free;
end;

{ TSettings }

constructor TSettings.Create(name: unicodestring; _s: TVList);
var sl: TStringList; i,j: integer; ol: TVList;
begin
    SetLength(s, _s.Count);
    for i := 0 to _s.high do
        s[i] := TSetting.Create(
            _s.L[i].sym[0],
            _s.L[i][1],
            extract_m_unit(_s.L[i]));

    fSettingsPanel := Tsettings_form.Create(change);
    fSettingsPanel.Caption := name;
    for i := 0 to high(s) do begin
        if s[i].options is TVList then begin
            ol := s[i].options as TVList;
            sl := TStringList.Create;
            for j := 0 to ol.high do sl.Add(ol.look[j].AsString+' '+s[i].m_unit);
            fSettingsPanel.add_blank(i, s[i].name, sl);
        end;
        if vpSymbol_FLOAT(s[i].options) then begin
            fSettingsPanel.add_blank_float(i,s[i].name);
        end;
    end;
end;

destructor TSettings.Destroy;
var i: integer;
begin
    for i := 0 to high(s) do s[i].Free;
    fSettingsPanel.Free;
end;

procedure TSettings.Change(sender: TObject);
var ss: TSettingSelection; i: integer;
begin
    if sender is TSettingSelection
    then begin
      ss := sender as TSettingSelection;


      if s[ss.i].options is TVList
      then s[ss.i].value := (s[ss.i].options as TVList).look[ss.o];

      if vpSymbol_FLOAT(s[ss.i].options)
      then begin
            s[ss.i].value.Free;
            s[ss.i].value := TVFloat.Create(ss.f);
      end;

      notify_change(fParent);
    end;
end;

procedure TSettings.Show;
begin
    fSettingsPanel.ShowOnTop;
end;

function TSettings.GetValue(name: unicodestring): TValue;
var i: integer;
begin
    for i := 0 to high(s) do
        if s[i].name=name then begin
            result := s[i].value;
            Exit;
        end;
    raise Exception.Create(name+' - не найдено');
end;

function TSettings.GetFloat(name: unicodestring): double;
var V: TValue;
begin
    V := GetValue(name);
    if tpReal(V)
    then result := (V as TVReal).F
    else raise Exception.Create(V.AsString+' - не число');
end;

function TSettings.GetString(name: unicodestring): unicodestring;
begin
    result := GetValue(name).AsString;
end;

function TSettings.AsList: TVList;
var i: integer;
begin
    result := TVList.Create;
    for i := 0 to high(s) do
        result.Add(TVList.Create([TVSymbol.Create(s[i].name), s[i].value.Copy]));
end;

procedure TSettings.Read(L: TVList);
var i,j,k: integer;
begin
    for i := 0 to L.high do
        for j := 0 to high(s) do
            if s[j].name=L.L[i].name[0] then begin
                s[j].value.Free;
                s[j].value := L.L[i][1];
                if
                break;
            end;
end;


{ TAccumulator }

function TAccumulator.MaxMode: TPointFloat;
var V: TValue;
begin
    V := call(fMaxModeExpr);
    if (not vpPoint(V))
    then raise Exception.Create(V.AsString+' ошибка в параметрах аккумулятора '+ model);
    result := fpoint((V as TVList).F[0], (V as TVList).F[1]);
end;

function TAccumulator.MinMode: TPointFloat;
var V: TValue;
begin
    V := call(fMinModeExpr);
    if (not vpPoint(V))
    then raise Exception.Create(V.AsString+' ошибка в параметрах аккумулятора '+ model);
    result := fpoint((V as TVList).F[0], (V as TVList).F[1]);
end;

constructor TAccumulator.Create(fn: unicodestring);
begin
  self.Read(fn);

  if not vpStatement_ACCUMULATOR(fDefinition)
  then raise Exception.Create(fn+' не является описанием аккумулятора');

  fMaxModeExpr:= assoc(sMaxMode, fDefinition);
  fMinModeExpr:= assoc(sMinMode, fDefinition);
end;

constructor TAccumulator.Create;
var V: TVList;
begin
    V := dlisp_read.read_from_string('(аккумулятор '+
        '(модель "Аккумулятор") '+
        '(максимальный-режим \(242 0,037)) '+
        '(минимальный-режим  \(198 0.055)))') as TVList;
    self.Read(V);

    fMaxModeExpr:= assoc(sMaxMode, fDefinition);
    fMinModeExpr:= assoc(sMinMode, fDefinition);
end;

destructor TAccumulator.Destroy;
begin
    fMaxModeExpr.Free;
    fMinModeExpr.Free;
    inherited Destroy;
end;

{ TAdjustableObject }

function TAdjustableObject.call(expr: TVList): TValue;
var frame_start, i: integer;
begin try
    frame_start := flow.stack.Count;
    for i := 0 to high(settings.s) do
        flow.stack.new_var(settings.s[i].name, settings.s[i].value.Copy, true);
    result := flow.oph_block(expr,0,false);
finally
    flow.stack.clear_frame(frame_start);
end; end;

procedure TAdjustableObject.ShowSettingsPanel;
begin
    settings.fSettingsPanel.ShowOnTop;
end;


procedure TAdjustableObject.Read(fn: unicodestring);
var stream: TLFileStream; V: TValue;
begin
    model := ExtractFileName(fn);
    stream := TLFileStream.Create(fn, fmOpenRead, lisia_charset.seUTF8);
try
    V := dlisp_read.read(stream);

    if not (V is TVList)
    then begin V.Free;  Exception.Create(fn+' нарушена структура'); end
    else self.read(V as TVList);

finally
    stream.Free;
end;
end;


procedure TAdjustableObject.Read(_d: TVList);
var  m, s: TVList; i: integer;
begin try
    fDefinition := _d;

    m := assoc(sModel, fDefinition);
    if (m.Count=1) and tpString(m.look[0]) then model := m.s[0];

    s := assoc(sSettings, fDefinition);
    settings := TSettings.Create(model, s);
finally
    m.Free;
    s.Free;
end;end;

destructor TAdjustableObject.Destroy;
var i: integer;
begin
    //fSettingsPanel.Free;
    settings.Free;
    fDefinition.Free;
//    for i := 0 to high(settings) do settings[i].Free;
    inherited Destroy;
end;

{ TFuse }


function TFuse.CalcCurve: TPointsFloat;
var V: TValue;
begin try
    V := call(fCurveExpr);
    if not vpListOfPoints(V) then raise Exception.Create('Ошибка в характеристике '+model);
    result := list_to_points(V as TVList);
finally
    V.Free;
end; end;



constructor TFuse.Create(fn: unicodestring);
begin
    self.Read(fn);

    if not vpStatement_FUSE(fDefinition)
    then raise Exception.Create(fn+' не является описанием защитного аппарата');

    fCurveExpr:= assoc(sCurve, fDefinition);
end;

destructor TFuse.Destroy;
begin
    fCurveExpr.Free;
    inherited Destroy;
end;

{ TSetting }

constructor TSetting.Create(n: TVSymbol; o: TValue; _u: unicodestring);
begin
    name := n.name;
    options := o;
    value := nil;
    if o is TVList then value := (o as TVList).look[0];
    if vpSymbol_FLOAT(o) then value := TVFloat.Create(1);
    m_unit := _u;
end;

destructor TSetting.Destroy;
begin
    options.Free;
end;

initialization
    flow := TEvaluationFlow.Create(nil);
    flow.oph_execute_file(ExtractFilePath(Application.ExeName)+'fuse2.lisya');
    sModel := TVSymbol.Create('МОДЕЛЬ');
    sSettings := TVSymbol.Create('УСТАВКИ');
    sCurve := TVSymbol.Create('ХАРАКТЕРИСТИКА');
    sMaxMode := TVSymbol.Create('МАКСИМАЛЬНЫЙ-РЕЖИМ');
    sMinMode := TVSymbol.Create('МИНИМАЛЬНЫЙ-РЕЖИМ');
    sFloat := TVSymbol.Create('ЧИСЛО');

finalization
    flow.Free;
    sModel.Free;
    sSettings.Free;
    sCurve.Free;
    sMaxMode.Free;
    sMinMode.Free;
    sFloat.Free;

end.

