﻿unit main;

{$mode delphi}{$H+}

interface

uses
    {$IFDEF LINUX}
    cwstring,
    {$ENDIF}
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    StdCtrls, ComCtrls, ActnList, Contnrs, math, cirquits
    , lisya_interface
    , geometry
    , settings
    ;

type

    TLCanvasException = exception;


    { TGO }

    TGO = class
        fcolor: TColor;
        ftransform: TMatrix;
        procedure paint; virtual; abstract;
    end;

    { TGFuse }

    TGFuse = class(TGO)
        points: TPointsFloat;
        procedure paint; override;
    end;

    { TGSC }

    { TGFault }

    TGFault = class (TGO)
        max_points: TPointsFloat;
        min_points: TPointsFloat;
        procedure paint; override;
    end;

    { TElt }

    TElt = class
        fNode: TTreeNode;
        g: TGO;
        procedure ShowSettingsPanel; virtual;
        procedure Refresh; virtual;
        constructor Create(_g: TGO);
        destructor Destroy;
        function AsRecord: TElementRec; virtual; abstract;
    end;

    { TEFuse }

    TEFuse = class (TElt)
        f: TFuse;
        procedure ShowSettingsPanel; override;
        procedure Refresh; override;
        constructor Create(_f: tFuse);
        destructor Destroy;
        function AsRecord: TElementRec; override;
    end;


    { TEAccumulator }

    TEAccumulator = class(TElt)
        a: TAccumulator;
        max_mode, min_mode: TPointFloat;
        procedure ShowSettingsPanel; override;
        procedure Refresh; override;
        constructor Create(_a: TAccumulator);
        destructor Destroy;
        function AsRecord: TElementRec; override;
    end;


    { TECable }

    TECable = class (TElt)
        settings: TSettings;
        L, s, ro: double;
        procedure ShowSettingsPanel; override;
        procedure Refresh; override;
        constructor Create;
        destructor Destroy;
        function AsRecord: TElementRec; override;
    end;

    { TEBus }

    TEBus = class (TElt)
        function AsRecord: TElementRec; override;
    end;

    { TEFault }

    TEFault = class (TElt)
        constructor Create;
        procedure Refresh; override;
        function AsRecord: TElementRec; override;
    end;


    TCirquitElement = record
        R, Kt, t, q: double;
    end;


    { Tmain_form }

    Tmain_form = class(TForm)
        Action_save_as: TAction;
        Action_Add_Fault: TAction;
        Action_Add_Bus: TAction;
        Action_Add_Cable: TAction;
        Action_Open_Fuse: TAction;
        Action_Open_Accumulator: TAction;
        Action_change: TAction;
        ActionList1: TActionList;
        OpenAccumulator: TOpenDialog;
        OpenFuse: TOpenDialog;
        SaveDialog: TSaveDialog;
        procedure Action_Add_BusExecute(Sender: TObject);
        procedure Action_Add_CableExecute(Sender: TObject);
        procedure Action_Add_FaultExecute(Sender: TObject);
        procedure Action_changeExecute(Sender: TObject);
        procedure Action_Open_AccumulatorExecute(Sender: TObject);
        procedure Action_Open_FuseExecute(Sender: TObject);
        procedure Action_save_asExecute(Sender: TObject);

        procedure FormClick(Sender: TObject);
        procedure FormCreate(Sender: TObject);
        procedure FormDblClick(Sender: TObject);
        procedure FormDestroy(Sender: TObject);
        procedure FormPaint(Sender: TObject);
        procedure FormResize(Sender: TObject);
        procedure ShowSettingsPanel(n: TTreeNode);
        //procedure ChangeFuse(
    private

    public
        procedure AddObject(name: unicodestring; e: TElt);
        procedure SaveToFile(fn: unicodestring);
        procedure LoadFromFile(fn: unicodestring);

        { public declarations }
    end;



var
    main_form: Tmain_form;



    lo_x: real = -1;
    hi_x: real = 1;
    a_x: real = 2;
    lo_y: real = -1;
    hi_y: real = 1;
    a_y: real = 2;
    cw,ch: integer;
    frame: integer;
    cnv: TCanvas;
    equal_scale: boolean = false;
    color: TColor = clBlack;
    transform: TMatrix = ((1,0,0),(0,1,0),(0,0,1));
    tm: TMatrix = ((1,0,0),(0,1,0),(0,0,1));

    bx: integer = 30; //отступы для графиков
    by: integer = 15;


const
    Cu_ro = 0.0175;
    Al_ro = 0.0285;
    Cu_dens = 8960;
    Al_dens = 2700;
    Cu_Q = 385;
    Al_Q = 930;
    Cu_Kt = 0.0038;
    Al_Kt = 0.0043;
    Cu_I2dt = 1000000*Cu_ro/(Cu_dens*Cu_Q); //коэффициент роста температуры под токои
    Al_I2dt = 1000000*Al_ro/(Al_dens*Al_Q);


procedure calc(n: TTreeNode; out max_mode: TPointsFloat; out min_mode: TPointsFloat);

type TProcOnElt = procedure (e: TElt);

procedure AlongTreeDown(n: TTreeNode; action: TProcOnElt);
procedure RefreshFault(e: TElt);

implementation


{$R *.lfm}

procedure error(const msg: unicodestring);
begin
    raise TLCanvasException.Create(msg);
end;


function lg(f: real): real; inline;
begin
    if f>0 then result := {logn(1.5,f)} log2(f) else result := f;
end;


procedure set_area(lx,hx,ly,hy: real);
begin
    lo_x := lx;
    hi_x := hx;
    lo_y := ly;
    hi_y := hy;
    a_x := hi_x - lo_x;
    a_y := hi_y - lo_y;
    cw := main_form.ClientWidth;
    ch := main_form.ClientHeight;

    frame := min(cw, ch);
end;

function scaleX(x: real): integer; inline;
var wx: real; _x: real;
begin
    _x := lg(x);
    wx := cw*(_x - lo_x)/a_x;
    result := Round(wx)+bx;
end;

function scaleY(y: real): integer;
var wy: real; _y: real;
begin
    _y := lg(y);
    wy := ch - ch*(_y - lo_y)/a_y;
    result := Round(wy)-by;
end;

function scale(x, y: real): TPoint;  overload;
var wx,wy: real; _x, _y: real;
begin
    _x := lg(x);
    _y := lg(y);
    wx := cw*(_x - lo_x)/a_x;
    wy := ch - ch*(_y - lo_y)/a_y;
    result := point(Round(wx)+bx, Round(wy)-by);
end;

function scale(_p: TPointFloat): TPoint;  overload;
var wx,wy: real; p: TPointFloat;
begin
    p := mul(_p,tm);
    p.x := lg(p.x);
    p.y := lg(p.y);
    wx := cw*(p.x - lo_x)/a_x;
    wy := ch - ch*(p.y - lo_y)/a_y;
    result := point(Round(wx)+bx, Round(wy)-by);
end;

{ TEBus }

function TEBus.AsRecord: TElementRec;
begin
    result := TElementRec.Create(fNode.Text,'шина',nil);
end;

{ TGFault }

procedure TGFault.paint;
var i: integer;
begin
    cnv.Pen.Width:=3;
    cnv.Pen.Color:=clRed;
    if length(max_points)>0 then begin
        cnv.MoveTo(scale(max_points[0]));
        for i := 1 to high(max_points) do cnv.LineTo(scale(max_points[i]));
    end;
    if Length(min_points)>0 then begin
        cnv.MoveTo(scale(min_points[0]));
        for i := 1 to high(min_points) do cnv.LineTo(scale(min_points[i]));
    end;
end;

{ TEFault }

constructor TEFault.Create;
begin
    inherited Create(TGFault.Create);
end;

procedure TEFault.Refresh;
begin
    calc(fNode, (g as TGFault).max_points, (g as TGFault).min_points);
end;

function TEFault.AsRecord: TElementRec;
begin
    result := TElementRec.Create(fNode.Text,'КЗ',nil);
end;


{ TECable }

procedure TECable.ShowSettingsPanel;
begin
    settings.Show;
end;

procedure TECable.Refresh;
var m: unicodestring;
begin
    L := settings.GetFloat('Длина');
    s := settings.GetFloat('Сечение');
    m := settings.GetString('Материал');
    if m='Cu' then ro := 0.0175;
    if m='Al' then ro := 0.0285;
    AlongTreeDown(fNode, RefreshFault);
    main_form.Repaint;
end;

constructor TECable.Create;
begin
    inherited Create(nil);
    settings := cable_settings;
    settings.fParent := self;
    L := 1;
    s := 1;
    ro := 0.0175;
end;

destructor TECable.Destroy;
begin
    settings.Free;
end;

function TECable.AsRecord: TElementRec;
begin
    result := TElementRec.Create(fNode.Text,'кабель', settings);
end;

{ TEAccumulator }

procedure TEAccumulator.ShowSettingsPanel;
begin
    a.ShowSettingsPanel;
end;

procedure TEAccumulator.Refresh;
begin
    max_mode := a.MaxMode;
    min_mode := a.MinMode;
end;

constructor TEAccumulator.Create(_a: TAccumulator);
begin
    inherited Create(nil);
    a := _a;
    a.settings.fParent := self;
    Refresh;
end;

destructor TEAccumulator.Destroy;
begin
    a.Free;
end;

function TEAccumulator.AsRecord: TElementRec;
begin
    result := TElementRec.Create(fNode.Text,a);
end;


{ TEFuse }

procedure TEFuse.ShowSettingsPanel;
begin
    f.ShowSettingsPanel;
end;

procedure TEFuse.Refresh;
begin
    SetLength((g as TGFuse).points, 0);
    (g as TGFuse).points := f.CalcCurve;
end;

constructor TEFuse.Create(_f: tFuse);
begin
    inherited Create(TGFuse.Create);
    f := _f;
    f.settings.fParent := self;
    Refresh;
end;

destructor TEFuse.Destroy;
begin
  f.Free;
end;

function TEFuse.AsRecord: TElementRec;
begin
    result := TElementRec.Create(fNode.Text,f);
end;

{ TElt }

procedure TElt.ShowSettingsPanel;
begin
    //у некоторых элементов нет панели параметров
end;

procedure TElt.Refresh;
begin
    //соответственно и параметров тоже нет
end;

constructor TElt.Create(_g: TGO);
begin
    fNode := nil;
    g := _g;
end;

destructor TElt.Destroy;
begin
    g.Free;
end;

{ TGFuse }

procedure TGFuse.paint;
var i: integer; ip: array of TPoint;
begin
    cnv.Pen.Width:=1;
    cnv.Pen.Color:=clBlack;
    cnv.Brush.Style:=bsSolid;
    cnv.Brush.Color:=$800000FF;
    SetLength(ip, Length(points));
    for i := 0 to high(points) do ip[i]:=scale(points[i]);

    cnv.Polygon(ip);
    SetLength(ip, 0);
end;


procedure calc(n: TTreeNode; out max_mode: TPointsFloat; out min_mode: TPointsFloat);
var cirquit: array of TCirquitElement; p: TTreeNode; ec: TECable; ea: TEAccumulator;
    t, dt, R, I: double;
    it,j: integer;
const iterations_count = 30;
begin
    SetLength(cirquit, 0);
    p := n;
    while p<>nil do begin
        if TElt(p.Data) is TECable then begin
            ec := TElt(p.Data) as TECable;
            SetLength(cirquit, Length(cirquit)+1);
            cirquit[high(cirquit)].R := ec.ro*ec.L*2/ec.s;
            cirquit[high(cirquit)].t:=20;
            cirquit[high(cirquit)].Kt:=Cu_Kt;
            cirquit[high(cirquit)].q := Cu_I2dt;
        end;
        p := p.Parent;
    end;

    ea := TElt(TV.Items[0].Data) as TEAccumulator;
    SetLength(max_mode, iterations_count);
    t := 0;
    dt := 0.001;
    for it := 0 to iterations_count-1 do begin
        R := 0;
        for j := 0 to high(cirquit) do R := R + cirquit[j].R*(1+cirquit[j].Kt*cirquit[j].t);
        I := ea.max_mode.U/(ea.max_mode.R+R);
        max_mode[it].I:=I;
        max_mode[it].t:=t;
        t := t+dt;
        for j := 0 to high(cirquit) do cirquit[j].t := cirquit[j].t + cirquit[j].q*(I**2)*dt;
        dt := dt*1.4;
    end;

    for it := 0 to high(cirquit) do cirquit[it].t := 20;

    SetLength(min_mode, iterations_count);

    t := 0;
    dt := 0.001;
    for it := 0 to iterations_count-1 do begin
        R := 0;
        for j := 0 to high(cirquit) do R := R + cirquit[j].R*(1+cirquit[j].Kt*cirquit[j].t);
        R := R*(0.029*ln(R)+0.375);
        I := ea.min_mode.U/(ea.min_mode.R+R);
        min_mode[it].I:=I;
        min_mode[it].t:=t;
        t := t+dt;
        for j := 0 to high(cirquit) do cirquit[j].t := cirquit[j].t + cirquit[j].q*(I**2)*dt;
        dt := dt*1.4;
    end;
end;

procedure AlongTreeDown(n: TTreeNode; action: TProcOnElt);
var ch: tTreeNode;
begin
    if n.Data<>nil then action(TElt(n.Data));

    ch := n.GetFirstChild;
    while ch<>nil do begin
        AlongTreeDown(ch, action);
        ch := n.GetNextChild(ch);
    end;
end;

procedure RefreshFault(e: TElt);
begin
    if e is TEFault then (e as TEFault).Refresh;
end;

procedure Tmain_form.FormClick(Sender: TObject);
begin
    caption := FloatToStr(logn(1.5,20000));
end;

procedure Tmain_form.FormCreate(Sender: TObject);
var a: TAccumulator; ea: TEAccumulator;
begin
    cnv := main_form.Canvas;
    set_area(0,13,-5,10);
    cirquits_form := Tcirquits_form.Create(self);
    cirquits_form.SettingsOpen:=ShowSettingsPanel;

    cirquits_form.Select_Accumulator.OnClick:= Action_Open_accumulatorExeCute;
    cirquits_form.AddBus:=Action_Add_BusExecute;
    cirquits_form.AddCable:=Action_Add_CableExecute;
    cirquits_form.AddFuse:=Action_Open_FuseExecute;
    cirquits_form.AddFault:=Action_Add_FaultExecute;

    a := TAccumulator.Create;
    ea := TEAccumulator.Create(a);
    TV.Select(TV.Items.AddObject(nil, a.model, ea));
    ea.fNode := TV.Items[0];

    lisya_interface.notify_change:=Tmain_form.Action_changeExecute;

    LoadFromFile('work.lisya');
end;

procedure Tmain_form.FormDblClick(Sender: TObject);
begin
    cirquits_form.Show;
end;



procedure Tmain_form.Action_changeExecute(Sender: TObject);
begin
    if sender is TElt then (sender as TElt).Refresh;
    if sender is TEFuse then main_form.Repaint;
end;

procedure Tmain_form.Action_Add_CableExecute(Sender: TObject);
var c: TECable;
begin
    c := TECable.Create;
    AddObject('Кабель', c);
    c.ShowSettingsPanel;
end;

var fault_n: integer = 0;
procedure Tmain_form.Action_Add_FaultExecute(Sender: TObject);
var f: TEFault;
begin
    Inc(fault_n);
    f := TEFault.Create;
    AddObject('К'+IntToStr(fault_n),f);
    f.Refresh;
    Paint;
end;

var bus_n: integer = 0;
procedure Tmain_form.Action_Add_BusExecute(Sender: TObject);
begin
    Inc(bus_n);
    AddObject(IntToStr(bus_n)+'С', TEBus.Create(nil))
end;

procedure Tmain_form.Action_Open_AccumulatorExecute(Sender: TObject);
var a: TAccumulator;
begin
    if OpenAccumulator.Execute then begin
        a := TAccumulator.Create(OpenAccumulator.FileName);
        TElt(TV.Items[0].Data).Free;
        TV.Items[0].Data:=TEAccumulator.Create(a);
        TV.Items[0].Text := a.model;
    end;
end;

procedure Tmain_form.Action_Open_FuseExecute(Sender: TObject);
var f: TFuse;
begin
    if OpenFuse.Execute then begin
        f := TFuse.Create(OpenFuse.FileName);
        AddObject(f.model,TEFuse.Create(f));
        main_form.Paint;
    end;
end;

procedure Tmain_form.Action_save_asExecute(Sender: TObject);
begin
    SaveToFile('work.lisya');
end;



procedure Tmain_form.FormDestroy(Sender: TObject);
begin

end;

procedure paint_cirquit(n: TTreeNode);
var e: TElt; i: integer; ch: TTreeNode;
begin
    if n=nil then Exit;
    e := TElt(n.Data);
    if e.g<>nil then e.g.paint;
    ch := n.GetFirstChild;
    while ch<>nil do begin
        paint_cirquit(ch);
        ch := n.GetNextChild(ch);
    end;
end;

const x_marks: array[0..18] of integer = (2,4,6,10,16,25,32,40,50,63,80,100,125,
    250,400,630,1000,2000,4000);

procedure Tmain_form.FormPaint(Sender: TObject);
var i, px, py: integer; style: TTextStyle;
    procedure Y_grid(y: float);
    var py: integer;
    begin
        py := scaleY(y);
        cnv.Line(bx,py,cnv.Width,py);
        //cnv.TextOut(2,py-12,FloatToStr(y));
        cnv.TextRect(Rect(0,py-10,bx-2,15),2,py-10,FloatToStr(y),style);
    end;

    procedure X_grid(x: integer);
    var px: integer;
    begin
        px := scaleX(x);
        cnv.Line(px,0,px, cnv.Height-by);
    end;

    procedure X_mark(x: integer);
    var px: integer;
    begin
        px := scaleX(x);
        cnv.TextOut(px,cnv.Height-by+2,IntToStr(x));
    end;

begin
    main_form.BeginFormUpdate;
    //cnv.Brush.Color:=clWhite;
    //cnv.Brush.Style:=bsSolid;
    //cnv.FillRect(cnv.ClipRect);
    //cnv.Pen.Color:= clBlack;
    //cnv.Brush.Style:=bsClear;

    //сетка
    style.Alignment:=taRightJustify;
    style.Clipping:=true;
    style.EndEllipsis:=false;
    style.ExpandTabs:=false;
    style.Layout:=tlTop;
    style.Opaque:=true;
    style.RightToLeft:=false;
    style.ShowPrefix:=false;
    style.SingleLine:=true;
    style.SystemFont:=false;
    style.Wordbreak:=false;

    cnv.Brush.Style:=bsClear;
    cnv.Pen.Color:=clgray;
    cnv.Pen.Width:=1;
    for i := 1 to 12 do begin
        px := scaleX(10**i);
        //cnv.Line(px,0,px, cnv.Height-by);
        cnv.TextOut(px,cnv.Height-by+2,IntToStr(10**i));
    end;

    for i := 0 to high(x_marks) do X_grid(x_marks[i]);

    Y_grid(300);
    Y_grid(120);
    Y_grid(60);
    Y_grid(30);
    Y_grid(15);
    for i := -1 to 3 do Y_grid(2.0**i);
    Y_grid(0.3);
    Y_grid(0.2);
    Y_grid(0.1);


    if TV<>nil then paint_cirquit(TV.Items.GetFirstNode);

    //оси
    cnv.Pen.Color:=clBlack;
    cnv.Line(bx,cnv.Height-by,cnv.Width,cnv.Height-by);
    cnv.Line(bx,cnv.Height-by,bx,0);

    cnv.Brush.Style:=bsClear;
    for i := 0 to high(x_marks) do X_mark(x_marks[i]);

    main_form.EndFormUpdate;
end;

procedure Tmain_form.FormResize(Sender: TObject);
begin
    set_area(lo_x, hi_x, lo_y, hi_y);
end;

procedure Tmain_form.ShowSettingsPanel(n: TTreeNode);
begin
    if n.Data<> nil then TElt(n.Data).ShowSettingsPanel;
end;

procedure Tmain_form.AddObject(name: unicodestring; e: TElt);
begin
    e.fNode := TV.Items.AddChildObject(TV.Selected, name, e);
end;


function save_records(n: TTreeNode): TElementRec;
var ch: TTreeNode;
begin
    result := TElt(n.Data).AsRecord;
    ch := n.GetFirstChild;
    while ch<>nil do begin
        result.AddChild(save_records(ch));
        ch := n.GetNextChild(ch);
    end;
end;

procedure Tmain_form.SaveToFile(fn: unicodestring);
var r: TElementRec;
begin
    r := save_records(TV.Items[0]);
    r.SaveToFile(fn);
    r.Free;
end;

procedure load_records(n: TTreeNode; r: TElementRec);
begin
    n.Text:=r.name;
end;

procedure Tmain_form.LoadFromFile(fn: unicodestring);
var r: TElementRec;
begin
    r := TElementRec.LoadFromFile(fn);

end;

end.


